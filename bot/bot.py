from discord.ext import commands
from discord.utils import get
import config

bot = commands.Bot(command_prefix='.')
bot.remove_command('help')


@bot.event
async def on_ready():
    print(f'Ready and logged in as {bot.user}')


@bot.command(name='help')
@commands.guild_only()
async def help_cmd(ctx):
    channel = get(ctx.guild.text_channels, name='help')
    await ctx.send(f'Please refer to {channel.mention} for help.')


EXTENSIONS = ['bot.friend_code',
              'bot.game',
              'bot.play',
              'bot.user',
              'bot.verify']


def run():
    for e in EXTENSIONS:
        bot.load_extension(e)
    bot.run(config.BOT_TOKEN)
