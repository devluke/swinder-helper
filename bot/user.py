import json
from pathlib import Path
import discord
from discord.ext import commands


class UserCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='rep')
    @commands.guild_only()
    async def rep_cmd(self, ctx, player: discord.User = None):
        if player is None:
            player = ctx.author
        user = SwinderUser(player.id)
        name = 'Your' if player == ctx.author else f"{player.mention}'s"
        await ctx.send(f'{name} reputation is `{user.reputation}`.')


class SwinderUser:
    DEFAULT_USER = {
        'friend_code': 'unknown',
        'verify_code': '',
        'reputation': 0
    }

    def __init__(self, user_id):
        self.user_id = user_id
        self.path = Path('storage', 'users', f'{user_id}.json')
        if not self.path.exists():
            self.path.write_text(json.dumps(self.DEFAULT_USER))
        data = json.loads(self.path.read_text())
        self.friend_code = data['friend_code']
        self.verify_code = data['verify_code']
        self.reputation = data['reputation']

    async def save(self):
        data = {
            'friend_code': self.friend_code,
            'verify_code': self.verify_code,
            'reputation': self.reputation
        }
        self.path.write_text(json.dumps(data))


def setup(bot):
    bot.add_cog(UserCog(bot))
