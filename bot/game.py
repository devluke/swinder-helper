from discord.ext import commands
from discord.utils import get


class GameCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.channel.category.name != 'Games' or '@game' not in message.clean_content.split():
            return
        game = message.channel.topic.split(': ', 1)[1]
        game_role = get(message.guild.roles, name=game)
        await message.channel.send(game_role.mention)


def setup(bot):
    bot.add_cog(GameCog(bot))
