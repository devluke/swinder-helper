import json
from pathlib import Path
import time
import discord
from discord.errors import Forbidden
from discord.ext import commands
from discord.utils import get
from bot.user import SwinderUser


# noinspection PyMethodParameters
class PlayCog(commands.Cog):
    game_emoji = '<:swinder:592770788852105216>'
    rep_yes_emoji = '👍'
    rep_no_emoji = '👎'

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if (message.author == self.bot.user and
                message.type == discord.MessageType.pins_add):
            await message.delete()

    async def game_player_change(self, payload, add=True):
        if str(payload.emoji) != self.game_emoji:
            return
        channel = self.bot.get_channel(payload.channel_id)
        if payload.guild_id is None:
            return
        games_category = get(channel.guild.categories, name='Games')
        if channel.category != games_category:
            return
        message = await channel.fetch_message(payload.message_id)
        reactors = await message.reactions[0].users().flatten()
        if (message.author != self.bot.user or not message.channel_mentions or self.game_emoji not in message.content or
                self.bot.user not in reactors):
            return
        private_channel = message.channel_mentions[-1]
        private_game = PrivateGame(self.bot, private_channel.name)
        member = self.bot.get_guild(payload.guild_id).get_member(payload.user_id)
        if member.id in [self.bot.user.id, private_game.host]:
            return
        if add:
            await private_game.add_player(member)
        else:
            await private_game.remove_player(member)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        await self.game_player_change(payload)
        channel = await self.bot.fetch_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        user = self.bot.get_user(payload.user_id)
        if (str(payload.emoji) not in [self.rep_yes_emoji, self.rep_no_emoji] or not message.raw_mentions or
                user == self.bot.user or channel.type != discord.ChannelType.private):
            return
        reactors = await message.reactions[0].users().flatten()
        if message.author != self.bot.user or self.bot.user not in reactors:
            return
        swinder_user = SwinderUser(message.raw_mentions[0])
        if str(payload.emoji) == self.rep_yes_emoji:
            swinder_user.reputation += 1
        else:
            swinder_user.reputation -= 1
        await swinder_user.save()
        await message.delete()

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        await self.game_player_change(payload, False)

    def play_cmd_check(ctx: commands.Context):
        category = get(ctx.guild.categories, name='Games')
        return ctx.channel.category == category

    @commands.command(name='play')
    @commands.check(play_cmd_check)
    @commands.cooldown(1, 600, commands.BucketType.user)
    @commands.guild_only()
    async def play_cmd(self, ctx):
        private = get(ctx.guild.categories, name='Private Games')
        code = str(int(time.time()))
        game = ctx.channel.topic.split(': ', 1)[1]
        text_overwrites = {
            ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False),
            ctx.author: discord.PermissionOverwrite(read_messages=True)
        }
        text_channel = await private.create_text_channel(code,
                                                         overwrites=text_overwrites,
                                                         topic=f'Private game channel for {game}')
        voice_overwrites = {
            ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False, connect=False),
            ctx.author: discord.PermissionOverwrite(read_messages=True, connect=True)
        }
        voice_channel = await private.create_voice_channel(code,
                                                           overwrites=voice_overwrites)
        game_role = get(ctx.guild.roles, name=game)
        msg = await ctx.send(f'''
{ctx.author.mention} is hosting {game_role.mention}.

React to this message with {self.game_emoji} to join.
If you wish to leave this game, simply remove your reaction.

{text_channel.mention}''')
        await msg.add_reaction(self.game_emoji)
        await msg.pin()
        private_game = PrivateGame(self.bot, code)
        private_game.players = [ctx.author.id]
        private_game.message = msg.id
        private_game.text_channel = text_channel.id
        private_game.voice_channel = voice_channel.id
        private_game.game_channel = ctx.channel.id
        private_game.host = ctx.author.id
        private_game.game = game
        await private_game.save()
        await ctx.message.delete()

    def private_game_check(ctx: commands.Context):
        category = get(ctx.guild.categories, name='Private Games')
        return ctx.channel.category == category

    @commands.command(name='info')
    @commands.check(private_game_check)
    @commands.guild_only()
    async def info_cmd(self, ctx):
        private_game = PrivateGame(self.bot, ctx.channel.name)
        await ctx.send(f'''**Game info:**
__Game:__ {private_game.game}
__Host:__ {self.bot.get_user(private_game.host).mention}
__Players:__ {', '.join([self.bot.get_user(user).mention for user in private_game.players])}''')

    @commands.command(name='stop')
    @commands.check(private_game_check)
    @commands.guild_only()
    async def stop_cmd(self, ctx):
        private_game = PrivateGame(self.bot, ctx.channel.name)
        host = self.bot.get_user(private_game.host)
        if ctx.author != host:
            await ctx.send(f'You must be the game host ({host.mention}) to stop the game.')
            return
        await private_game.stop()


class PrivateGame:
    DEFAULT_GAME = {
        'players': [],
        'message': 0,
        'text_channel': 0,
        'voice_channel': 0,
        'game_channel': 0,
        'host': 0,
        'game': ''
    }

    def __init__(self, bot, game_id):
        self.bot = bot
        self.game_id = game_id
        self.path = Path('storage', 'games', f'{game_id}.json')
        if not self.path.exists():
            self.path.write_text(json.dumps(self.DEFAULT_GAME))
        data = json.loads(self.path.read_text())
        self.players = data['players']
        self.message = data['message']
        self.text_channel = data['text_channel']
        self.voice_channel = data['voice_channel']
        self.game_channel = data['game_channel']
        self.host = data['host']
        self.game = data['game']

    async def save(self):
        data = {
            'players': self.players,
            'message': self.message,
            'text_channel': self.text_channel,
            'voice_channel': self.voice_channel,
            'game_channel': self.game_channel,
            'host': self.host,
            'game': self.game
        }
        self.path.write_text(json.dumps(data))

    async def ask_reputation(self, player):
        for p in self.players:
            if player.id == p:
                continue
            other = self.bot.get_user(p)
            try:
                msg = await player.send(f'''
You recently played {self.game} with **{other.mention}**.
Did you enjoy playing with them?''')
                await msg.add_reaction('👍')
                await msg.add_reaction('👎')
            except Forbidden:
                pass

    async def stop(self):
        channel = self.bot.get_channel(self.game_channel)
        message = await channel.fetch_message(self.message)
        await message.delete()
        await (self.bot.get_channel(self.text_channel)).delete()
        await (self.bot.get_channel(self.voice_channel)).delete()
        for p in self.players:
            user = self.bot.get_user(p)
            await self.ask_reputation(user)

    async def add_player(self, player):
        self.players.append(player.id)
        await self.save()
        text_channel = self.bot.get_channel(self.text_channel)
        await text_channel.set_permissions(player, read_messages=True)
        voice_channel = self.bot.get_channel(self.voice_channel)
        await voice_channel.set_permissions(player, read_messages=True, connect=True)
        player_user = SwinderUser(player.id)
        await text_channel.send(f'''
{player.mention} has joined the game.
Their friend code is `{player_user.friend_code}`.''')

    async def remove_player(self, player):
        self.players.remove(player.id)
        await self.save()
        text_channel = self.bot.get_channel(self.text_channel)
        await text_channel.set_permissions(player, overwrite=None)
        voice_channel = self.bot.get_channel(self.voice_channel)
        await voice_channel.set_permissions(player, overwrite=None)
        await text_channel.send(f'{player.mention} has left the game.')


def setup(bot):
    bot.add_cog(PlayCog(bot))
