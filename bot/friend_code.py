import discord
from discord.ext import commands
from discord.utils import get
from bot.user import SwinderUser


async def validate(code):
    return (code[0:3] == 'SW-' and
            code[3:7].isdigit() and
            code[7] == '-' and
            code[8:12].isdigit() and
            code[12] == '-' and
            code[13:17].isdigit())


class FriendCodeCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def send(self, user, guild):
        mention = self.bot.get_user(user.user_id).mention
        msg = f"{mention}'s friend code is `{user.friend_code}`."
        channel = get(guild.text_channels, name='friend-codes')
        async for message in channel.history():
            if message.content == msg:
                await message.delete()
        await channel.send(msg)

    @commands.command(name='fc')
    @commands.guild_only()
    async def fc_cmd(self, ctx, player: discord.User = None):
        if player is None:
            player = ctx.author
        user = SwinderUser(player.id)
        name = 'Your' if player == ctx.author else f"{player.mention}'s"
        await ctx.send(f'{name} friend code is `{user.friend_code}`.')

    @commands.command(name='setfc')
    @commands.guild_only()
    async def set_fc_cmd(self, ctx, new: str):
        if not await validate(new):
            await ctx.send('That friend code is not valid.')
            return
        user = SwinderUser(ctx.author.id)
        user.friend_code = new
        await user.save()
        await ctx.send('You have successfully set your friend code.')
        await self.send(user, ctx.guild)


def setup(bot):
    bot.add_cog(FriendCodeCog(bot))
