import datetime
from discord.errors import Forbidden
from discord.ext import commands
from discord.utils import get
from bot import friend_code
from bot.user import SwinderUser


class VerifyCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot:
            return
        guild = get(self.bot.guilds, name='Swinder')
        member = guild.get_member(message.author.id)
        if member is None:
            return
        guest_role = get(guild.roles, name='Guest')
        if guest_role not in member.roles:
            return
        if message.channel.type.name == 'text':
            verify_channel = get(guild.text_channels, name='verify')
            if message.channel != verify_channel:
                return
            await message.delete()
            if not await friend_code.validate(message.content):
                return
            verify_code = f'{str(hash(str(member)))[1:3]}{datetime.datetime.now().day}'
            user = SwinderUser(member.id)
            user.friend_code = message.content
            user.verify_code = verify_code
            await user.save()
            try:
                await member.send(f'''
The friend code you specified is valid!
To start using Swinder, please reply to this message with `{verify_code}`.
                ''')
            except Forbidden:
                pass
        elif message.channel.type.name == 'private':
            user = SwinderUser(member.id)
            if message.content != user.verify_code:
                return
            await member.remove_roles(get(guild.roles, name='Guest'))
            await member.add_roles(get(guild.roles, name='Player'))
            try:
                await member.send('You can now use Swinder! Have fun!')
            except Forbidden:
                pass
            log_channel = get(guild.text_channels, name='logs')
            await log_channel.send(f'{member.mention} has verified their friend code! `{user.friend_code}`')
            fc = self.bot.get_cog('FriendCodeCog')
            await fc.send(user, guild)


def setup(bot):
    bot.add_cog(VerifyCog(bot))
